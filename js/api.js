export const getUsers = async (id) => {
    const response = await fetch(`https://ajax.test-danit.com/api/json/users/${id}`)
    return response.json()};
      
export const userPosts = async ()=>{
    const response = await fetch(`https://ajax.test-danit.com/api/json/posts`)
    return response.json()}
