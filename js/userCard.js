import Element from "./element.js";
class Card extends Element {
    constructor(title, text, name, email, postId) {
      super();
      this.text = text;
      this.title = title;
      this.name = name;
      this.email = email;
      this.postId = postId;
    }
    render() {
      const cardContainer = document.querySelector(".container")
      const card = this.createElement("div",["card"]);
      card.innerHTML = `<h1>${this.title}</h1><p>${this.text}</p><span class ="name">${this.name}</span><a href="mailto:${this.email}">${this.email}</a><button class="remove-btn" id='${this.postId}'>X</button>`;
      cardContainer.append(card);
    }
  }
export  default Card