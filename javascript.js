import {getUsers,userPosts} from "./js/api.js"
import Card from "./js/userCard.js"
async function showUser() {
    const postsArr = await userPosts()
    postsArr.map(async ({body,title,userId,id}) => {
         await getUsers(userId)
         .then(async({name,email}) => {
                const card = new Card(title, body, name, email, id);
                card.render();
                [...document.querySelectorAll(".remove-btn")].map((item) => {
                    item.addEventListener("click",async (e) => {
                       await fetch(`https://ajax.test-danit.com/api/json/posts/${id}`, {
                            method: "DELETE",
                        }).then((response) => {
                            if (response.ok) {
                                delete e.target.closest("div").remove()
                            };
                        });
                    });
                });
         })         
    });     
};
showUser()